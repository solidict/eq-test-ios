//
//  GearAnimationViewController.swift
//  EQ-Test
//
//  Created by Selim Savsar on 14.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import Gifu

class GearAnimationViewController: UIViewController {
    
    var mainView: GearAnimationView! {
        guard isViewLoaded else { return nil }
        return view as! GearAnimationView
    }
    
    var results: [AnsweredQuestionModel] = []
    var finalResults: [ResultModel] = []
    
    public var questionStrategy: QuestionStrategy?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 2, delay: 0, options:.curveEaseInOut, animations: {
            self.mainView.bottomBackgroundBottomConstraint.constant = -150
            self.mainView.topBackgroundTopConstraint.constant = -150
            self.view.layoutIfNeeded()
            
            self.mainView.animationView.animate(withGIFNamed:"Gears_Version2") {
                print("Animated")
            }
            
        }) { (finished) in
            UIView.animate(withDuration: 2, delay: 2, options:.curveEaseInOut, animations: {
                self.mainView.bottomBackgroundBottomConstraint.constant = 0
                self.mainView.topBackgroundTopConstraint.constant = 0
                self.view.layoutIfNeeded()
                
                NetworkClient.evaluate(answeredQuestions: self.results) { (error, results) in
                    
                    if let error = error {
                        switch error {
                        case .logicError(let error):
                            print("error occurred: error: \(error)")
                            
                        case .statusCode(let statusCode):
                            print("error occurred: statusCode: \(statusCode)")
                        }
                        
                        return
                    }
                    
                    guard let results = results else {
                        return
                    }
                    self.finalResults = results
                    
                }
                
            }) { (finished) in
                self.goResultVC(results: self.finalResults)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func goResultVC(results: [ResultModel]) {
        
        let resultVC = ResultViewController()
        resultVC.results = results
        self.navigationController?.pushViewController(resultVC, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
