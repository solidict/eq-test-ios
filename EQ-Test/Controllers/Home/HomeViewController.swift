//
//  HomeViewController.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import JGProgressHUD

// MARK: - HomeViewController
class HomeViewController: UIViewController {

    // MARK: - Instance Properties
    var homeView: HomeView! {
        guard isViewLoaded else { return nil }
        return view as? HomeView
    }

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        configureButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configuration
    func configureButtons() {
        
        homeView.buttonStart.setTitle("START TEST", for: .normal)
        
        homeView.buttonStart.layer.borderWidth = 2.5
        homeView.buttonStart.layer.borderColor = UIColor.black.cgColor
        
        homeView.buttonStart.layer.shadowColor = UIColor.black.cgColor
        homeView.buttonStart.layer.shadowOpacity = 0.4
        homeView.buttonStart.layer.shadowOffset = CGSize.zero
        
        homeView.buttonFillProfile.setTitle("FILL PROFILE", for: .normal)
        
        homeView.buttonFillProfile.layer.borderWidth = 2.5
        homeView.buttonFillProfile.layer.borderColor = UIColor.black.cgColor
        
        homeView.buttonFillProfile.layer.shadowColor = UIColor.black.cgColor
        homeView.buttonFillProfile.layer.shadowOpacity = 0.4
        homeView.buttonFillProfile.layer.shadowOffset = CGSize.zero
    }
    
    // MARK: - Web Services
    func getQuestions() {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        NetworkClient.getQuestions { (error, questions) in
            
            hud.dismiss()
            if let error = error {
                switch error {
                case .logicError(let error):
                    print("error occurred: error: \(error)")
                    
                case .statusCode(let statusCode):
                    print("error occurred: statusCode: \(statusCode)")
                }
                
                return
            }
            
            guard let questions = questions,
                questions.count > 0 else {
                return
            }
            
            self.goQuestionVC(questions: questions)
        }
    }

    // MARK: - Actions
    @IBAction func buttonStartTapped(_ sender: Any) {
        
        let homeViewDescriptionController = HomeDescriptionViewController()
        self.navigationController?.pushViewController(homeViewDescriptionController, animated:true)
            
//        getQuestions()
    }
    
    @IBAction func buttonFillProfileTapped(_ sender: Any) {
        
        let profileVC = ProfileViewController()
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    // MARK: - Navigation
    func goQuestionVC(questions: [QuestionModel]) {
        
        let questionViewController = QuestionViewController()
        questionViewController.questionStrategy = SequentialQuestionStrategy(questions: questions)
        self.navigationController?.pushViewController(questionViewController, animated: true)
    }

}
