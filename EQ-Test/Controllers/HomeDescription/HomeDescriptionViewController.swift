//
//  HomeDescriptionViewController.swift
//  EQ-Test
//
//  Created by Selim Savsar on 13.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import JGProgressHUD

class HomeDescriptionViewController: UIViewController {
    
    // MARK: - Instance Properties
    var homeView: HomeDescription! {
        guard isViewLoaded else { return nil }
        return view as? HomeDescription
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureButton()
        configureView()
        // Do any additional setup after loading the view.
    }

    // MARK: - Configuration
    func configureButton() {
        
        homeView.buttonStart.setTitle("I'M READY", for: .normal)
        
        homeView.buttonStart.layer.borderWidth = 2.5
        homeView.buttonStart.layer.borderColor = UIColor.black.cgColor
        
        homeView.buttonStart.layer.shadowColor = UIColor.black.cgColor
        homeView.buttonStart.layer.shadowOpacity = 0.4
        homeView.buttonStart.layer.shadowOffset = CGSize.zero
    }
    
    func configureView() {
        
        homeView.descriptionBackground.layer.cornerRadius = 4.0
        homeView.descriptionBackground.layer.shadowColor = UIColor.black.cgColor
        homeView.descriptionBackground.layer.shadowOpacity = 0.4
        homeView.descriptionBackground.layer.shadowOffset = CGSize.zero
    }
    
    @IBAction func buttonReadyTapped(_ sender: Any) {
        getQuestions()
    }
    
    @IBAction func buttonBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Web Services
    func getQuestions() {
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        NetworkClient.getQuestions { (error, questions) in
            
            hud.dismiss()
            if let error = error {
                switch error {
                case .logicError(let error):
                    print("error occurred: error: \(error)")
                    
                case .statusCode(let statusCode):
                    print("error occurred: statusCode: \(statusCode)")
                }
                
                return
            }
            
            guard let questions = questions,
                questions.count > 0 else {
                    return
            }
            
            self.goQuestionVC(questions: questions)
        }
    }
    
    // MARK: - Navigation
    func goQuestionVC(questions: [QuestionModel]) {
        
        let questionViewController = QuestionViewController()
        questionViewController.questionStrategy = SequentialQuestionStrategy(questions: questions)
        self.navigationController?.pushViewController(questionViewController, animated: true)
    }
    

}
