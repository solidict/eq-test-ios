//
//  QuestionViewController.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import JGProgressHUD

// MARK: - QuestionViewController
class QuestionViewController: UIViewController {
    
    var audioPlayer: AVAudioPlayer?
    let generator = UIImpactFeedbackGenerator(style: .medium)
    
    // MARK: - Instance Properties
    var questionView: QuestionView! {
        guard isViewLoaded else { return nil }
        return view as? QuestionView
    }
    
    public var questionStrategy: QuestionStrategy?
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadPlayer()
        configureQuestion()

    }
    
    func loadPlayer() {
        do {
            if let fileURL = Bundle.main.path(forResource: "click", ofType: "m4a") {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
            } else {
                print("No file with specified name exists")
            }
        } catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureView()
        configureLabels()
        configureButtons()
        configureViewWithCurrentQuestion()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Configuration
    func configureView() {
        
        questionView.viewMain.layer.shadowColor = UIColor.black.cgColor
        questionView.viewMain.layer.shadowOpacity = 0.4
        questionView.viewMain.layer.shadowOffset = CGSize.zero
        
        questionView.viewOption1.layer.cornerRadius = 4.0
        questionView.viewOption2.layer.cornerRadius = 4.0
        questionView.viewOption3.layer.cornerRadius = 4.0
        questionView.viewOption4.layer.cornerRadius = 4.0
        questionView.viewOption5.layer.cornerRadius = 4.0

    }
    
    func configureLabels() {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        
        let attrString1 = NSMutableAttributedString(string: "👎👎\n1")
        attrString1.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString1.length))
        questionView.labelOption1.attributedText = attrString1
        questionView.labelOption1.textAlignment = .center

        let attrString2 = NSMutableAttributedString(string: "👎\n2")
        attrString2.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString2.length))
        questionView.labelOption2.attributedText = attrString2
        questionView.labelOption2.textAlignment = .center
        
        let attrString3 = NSMutableAttributedString(string: "🤔\n3")
        attrString3.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString3.length))
        questionView.labelOption3.attributedText = attrString3
        questionView.labelOption3.textAlignment = .center
        
        let attrString4 = NSMutableAttributedString(string: "👍\n4")
        attrString4.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString4.length))
        questionView.labelOption4.attributedText = attrString4
        questionView.labelOption4.textAlignment = .center
        
        let attrString5 = NSMutableAttributedString(string: "👍👍\n5")
        attrString5.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString5.length))
        questionView.labelOption5.attributedText = attrString5
        questionView.labelOption5.textAlignment = .center
    }
    
    func configureButtons() {
        
        questionView.buttonBack.setTitle("Prev", for: .normal)
        
        questionView.buttonForward.semanticContentAttribute = .forceRightToLeft
        questionView.buttonForward.setTitle("Next", for: .normal)
    }
    
    func configureViewWithCurrentQuestion() {
        
        changeViewColor(index: -1)
        
        let answeredQuestions = questionStrategy!.results()
        if let index = answeredQuestions.index(where: { $0.questionId == questionStrategy!.currentQuestion().id }) {
            
            questionView.buttonForward.isHidden = false
            
            switch answeredQuestions[index].answer {
            case 1:
                self.changeViewColor(index: 0)
            case 2:
                self.changeViewColor(index: 1)
            case 3:
                self.changeViewColor(index: 2)
            case 4:
                self.changeViewColor(index: 3)
            case 5:
                self.changeViewColor(index: 4)
            default:
                break
            }
            
        } else {
            questionView.buttonForward.isHidden = true
        }
        
    }
    
    
    func configureQuestion() {
        
        questionView.labelQuestionCount.text = questionStrategy!.questionIndexTitle()
        
        let question = questionStrategy!.currentQuestion()
        questionView.labelQuestion.text = question.text
        
        if question.imageUrl != "" {
//            questionView.imageViewQuestion.contentMode = .scaleToFill
            questionView.imageViewQuestion.sd_setIndicatorStyle(.gray)
            questionView.imageViewQuestion.sd_setShowActivityIndicatorView(true)
            questionView.imageViewQuestion.sd_setImage(with: URL(string: question.imageUrl), placeholderImage: nil, options: .highPriority, completed: nil)
        }
    }
    
    func changeViewColor(index: Int) {
        
        let views: [UIView] = [questionView.viewOption1, questionView.viewOption2, questionView.viewOption3, questionView.viewOption4, questionView.viewOption5]
        
        if index == -1 {
            
            for view in views {
                view.backgroundColor = UIColor(red: 229.0 / 255.0, green: 229.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
            }
            
        } else {
            
            for viewIndex in 0..<views.count {
                
                if viewIndex == index {
                    views[viewIndex].backgroundColor = UIColor(red: 255.0 / 255.0, green: 204.0 / 255.0, blue: 0.0, alpha: 1.0)
                } else {
                    views[viewIndex].backgroundColor = UIColor(red: 229.0 / 255.0, green: 229.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
                }
            }
        }
    }
    
    @objc func goForward() {
        
        guard questionStrategy!.isQuestionAnswered() else {
            print("Question not answered")
            self.questionView.isUserInteractionEnabled = true
            return
        }
        
        guard questionStrategy!.advanceToNextQuestion() else {
            print("Results: \(questionStrategy!.results())")
            print("Result page")
            self.questionView.isUserInteractionEnabled = false
            self.shrinkView()
            return
        }
        
        questionView.isUserInteractionEnabled = true
        configureViewWithCurrentQuestion()
        configureQuestion()
    }
    
    
    
    // MARK: - Actions
    @IBAction func buttonBackTapped(_ sender: Any) {
        
        guard questionStrategy!.advanceToPreviousQuestion() else {
                self.navigationController?.popViewController(animated: true)
                return
        }
        
        configureViewWithCurrentQuestion()
        configureQuestion()
    }
    
    @IBAction func buttonForwardTapped(_ sender: Any) {
        goForward()
    }
    
    @IBAction func buttonOptionTappedDown(_ sender: Any) {
        generator.impactOccurred()
    }
    
    @IBAction func buttonOption1Tapped(_ sender: Any) {

        changeViewColor(index: 0)
        
        let question = questionStrategy!.currentQuestion()
        let answeredQuestion = AnsweredQuestionModel(questionId: question.id, answer: 1)
        questionStrategy!.questionAnswered(answeredQuestion)
        
        audioPlayer?.play()
        questionView.isUserInteractionEnabled = false
        perform(#selector(self.goForward), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func buttonOption2Tapped(_ sender: Any) {
        
        changeViewColor(index: 1)

        let question = questionStrategy!.currentQuestion()
        let answeredQuestion = AnsweredQuestionModel(questionId: question.id, answer: 2)
        questionStrategy!.questionAnswered(answeredQuestion)
        
        audioPlayer?.play()
        questionView.isUserInteractionEnabled = false
        perform(#selector(self.goForward), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func buttonOption3Tapped(_ sender: Any) {
        
        changeViewColor(index: 2)

        let question = questionStrategy!.currentQuestion()
        let answeredQuestion = AnsweredQuestionModel(questionId: question.id, answer: 3)
        questionStrategy!.questionAnswered(answeredQuestion)
        
        audioPlayer?.play()
        questionView.isUserInteractionEnabled = false
        perform(#selector(self.goForward), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func buttonOption4Tapped(_ sender: Any) {
        
        changeViewColor(index: 3)

        let question = questionStrategy!.currentQuestion()
        let answeredQuestion = AnsweredQuestionModel(questionId: question.id, answer: 4)
        questionStrategy!.questionAnswered(answeredQuestion)
        
        audioPlayer?.play()
        questionView.isUserInteractionEnabled = false
        perform(#selector(self.goForward), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func buttonOption5Tapped(_ sender: Any) {
        
        changeViewColor(index: 4)

        let question = questionStrategy!.currentQuestion()
        let answeredQuestion = AnsweredQuestionModel(questionId: question.id, answer: 5)
        questionStrategy!.questionAnswered(answeredQuestion)
        
        audioPlayer?.play()
        questionView.isUserInteractionEnabled = false
        perform(#selector(self.goForward), with: nil, afterDelay: 0.5)
    }
    
    // MARK: - Navigation
    func shrinkView() {
        
        let animationView = GearAnimationViewController()
        animationView.results = questionStrategy!.results()
        self.navigationController?.pushViewController(animationView, animated: true)
    }

}
