//
//  ProfileViewController.swift
//  EQ-Test
//
//  Created by Serdar Turan on 4.10.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import AVFoundation
import ActionSheetPicker_3_0

// MARK: - ProfileViewController
class ProfileViewController: UIViewController {

    // MARK: - Instance Properties
    var profileView: ProfileView! {
        guard isViewLoaded else { return nil }
        return view as? ProfileView
    }
    
    var sections = ["DERSLER", "DAVRANIŞLAR", "TERCİHLER", "İLGİ ALANLARI"]
    var questions = [["1. Matematik dersine ilgiliyim", "2. Fizik dersine ilgiliyim", "3. Kimya dersine ilgiliyim", "4. Biyoloji dersine ilgiliyim", "5. Edebiyat dersine ilgiliyim", "6. Sosyal Bilgiler derslerine ilgiliyim", "7. Yabancı dil derslerine ilgiliyim.", "8. Sanat dersine ilgiliyim."],["1. İnsanları severim", "2. Yeni fikirler üretirim", "3. Paramın hesabını bilirim", "4. Her türlü aleti kullanırım"],["1. Evde yemek yemeği severim", "2. Dışarıda yemek yemeği severim", "3. Kitap okumayı severim", "4. Film seyretmeyi severim", "5. Oyun oynamayı severim"], ["1. Sanat", "2. Spor", "3. Müzik", "4. Drama", "5. Dans"]]
    var answers: [[Int?]] = [[nil, nil, nil, nil, nil, nil, nil, nil], [nil, nil, nil, nil], [nil, nil, nil, nil, nil], [nil, nil, nil, nil, nil]]
    let genders: [String] = ["Female", "Male"]
    let educations: [String] = ["Completed HS (Diploma or GED)", "Some college", "Completed bachelors degree", "Still in high school", "Other"]
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    
    var audioPlayer: AVAudioPlayer?
    let generator = UIImpactFeedbackGenerator(style: .medium)

    var pickerDate = ActionSheetDatePicker()
    var pickerGender = ActionSheetStringPicker()
    var pickerEducation = ActionSheetStringPicker()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        loadPlayer()

        configureScrollView()
        configureLabels()
        configureTextFields()
        configureButton()
        configureTableView()
        
        configureKeyboard()
    }
    
    // MARK: - Configuration
    func configureScrollView() {
        
        profileView.scrollView.layer.cornerRadius = 4.0
        profileView.scrollView.layer.shadowColor = UIColor.black.cgColor
        profileView.scrollView.layer.shadowOpacity = 0.4
        profileView.scrollView.layer.shadowOffset = CGSize.zero
    }
    
    func configureLabels() {
        
        profileView.labelHeader.text = "Profile"
        
        profileView.headerView.labelName.text = "Name"
        profileView.headerView.labelSurname.text = "Surname"
        profileView.headerView.labelGender.text = "Gender"
        profileView.headerView.labelBirthDate.text = "Birth Date"
        profileView.headerView.labelEducation.text = "Education"
    }
    
    func configureTextFields() {

        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor(red: 160 / 255, green: 160 / 255, blue: 160 / 255, alpha: 1.0),
            NSAttributedStringKey.font : UIFont(name: "GreycliffCF-MediumOblique", size: 17)!
        ]

        profileView.headerView.textFieldName.attributedPlaceholder =  NSAttributedString(string: "Name", attributes: attributes)
        profileView.headerView.textFieldSurname.attributedPlaceholder =  NSAttributedString(string: "Surname", attributes: attributes)
        profileView.headerView.textFieldGender.attributedPlaceholder =  NSAttributedString(string: "Gender", attributes: attributes)
        profileView.headerView.textFieldBirthDate.attributedPlaceholder =  NSAttributedString(string: "Birth Date", attributes: attributes)
        profileView.headerView.textFieldEducation.attributedPlaceholder =  NSAttributedString(string: "Education", attributes: attributes)
    }
    
    func configureButton() {
        
        profileView.footerView.buttonSave.layer.borderWidth = 2.5
        profileView.footerView.buttonSave.layer.borderColor = UIColor.black.cgColor
        
        profileView.footerView.buttonSave.layer.shadowColor = UIColor.black.cgColor
        profileView.footerView.buttonSave.layer.shadowOpacity = 0.4
        profileView.footerView.buttonSave.layer.shadowOffset = CGSize.zero
    }
    
    func configureTableView() {
        
        profileView.tableView.tableHeaderView = profileView.headerView
        profileView.tableView.tableFooterView = profileView.footerView
        
        profileView.tableView.estimatedRowHeight = 20
        profileView.tableView.rowHeight = UITableViewAutomaticDimension
        
        let profileHeader = UINib(nibName: "ProfileInformationTableViewCell", bundle: nil)
        profileView.tableView.register(profileHeader, forCellReuseIdentifier: "ProfileInformationTableViewCell")
        
        let sectionHeader = UINib(nibName: "ProfileSectionHeaderTableViewCell", bundle: nil)
        profileView.tableView.register(sectionHeader, forCellReuseIdentifier: "ProfileSectionHeaderTableViewCell")
        
        let question = UINib(nibName: "ProfileQuestionTableViewCell", bundle: nil)
        profileView.tableView.register(question, forCellReuseIdentifier: "ProfileQuestionTableViewCell")
        
    }
    
    func configureKeyboard() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = profileView.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        profileView.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        profileView.scrollView.contentInset = contentInset
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: - Actions
    @IBAction func buttonBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonDateTapped(_ sender: Any) {
        
        pickerDate = ActionSheetDatePicker(title: "Birth Date", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, value, any) in
            
            let dateFormatterOutput = DateFormatter()
            dateFormatterOutput.dateFormat = "d MMMM yyyy"
            
            self.profileView.headerView.textFieldBirthDate.text =  "\(dateFormatterOutput.string(from: value as! Date))"
            
        }, cancel: { (picker) in
            
        }, origin: self.profileView.headerView.textFieldBirthDate)
        
        pickerDate.tapDismissAction = .success
        pickerDate.setDoneButton(UIBarButtonItem(title: "Done", style: .done, target: nil, action: nil))
        pickerDate.setCancelButton(UIBarButtonItem(title: "Cancel", style: .done, target: nil, action: nil))
        pickerDate.show()
        
        dismissKeyboard()
    }
    
    @IBAction func buttonGenderTapped(_ sender: Any) {
        
        pickerGender = ActionSheetStringPicker(title: "Gender", rows: genders, initialSelection: 0, doneBlock: { (picker, index, value) in
            self.profileView.headerView.textFieldGender.text = "\(self.genders[index])"
        }, cancel: { (picker) in
            
        }, origin: self.profileView.headerView.textFieldGender)
        
        pickerGender.tapDismissAction = .success
        pickerGender.setDoneButton(UIBarButtonItem(title: "Done", style: .done, target: nil, action: nil))
        pickerGender.setCancelButton(UIBarButtonItem(title: "Cancel", style: .done, target: nil, action: nil))
        pickerGender.show()
        
        dismissKeyboard()
    }
    
    @IBAction func buttonEducationTapped(_ sender: Any) {
        
        pickerEducation = ActionSheetStringPicker(title: "Education", rows: educations, initialSelection: 0, doneBlock: { (picker, index, value) in
            self.profileView.headerView.textFieldEducation.text = "\(self.educations[index])"
        }, cancel: { (picker) in
            
        }, origin: self.profileView.headerView.textFieldEducation)
        
        pickerEducation.tapDismissAction = .success
        pickerEducation.setDoneButton(UIBarButtonItem(title: "Done", style: .done, target: nil, action: nil))
        pickerEducation.setCancelButton(UIBarButtonItem(title: "Cancel", style: .done, target: nil, action: nil))
        pickerEducation.show()
        
        dismissKeyboard()
    }
    
    
    @IBAction func buttonSaveTapped(_ sender: Any) {
        
        let alertView = UIAlertController(title: "Information", message: "Completed successfully.", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    
    func loadPlayer() {
        do {
            if let fileURL = Bundle.main.path(forResource: "click", ofType: "m4a") {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
            } else {
                print("No file with specified name exists")
            }
        } catch let error {
            print("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
    }
    
    // MARK: - Navigation

}

// MARK: - UITableViewDataSource
extension ProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return 1 + questions[section - 1].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInformationTableViewCell", for: indexPath) as! ProfileInformationTableViewCell
            return cell

        } else {
            
            if indexPath.row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSectionHeaderTableViewCell", for: indexPath) as! ProfileSectionHeaderTableViewCell
                cell.labelTitle.text = self.sections[indexPath.section - 1]
                return cell
                
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileQuestionTableViewCell", for: indexPath) as! ProfileQuestionTableViewCell
                cell.selectedIndexPath = indexPath
                cell.delegate = self
                
                cell.changeViewColor(index: -1)

                if answers[indexPath.section - 1][indexPath.row - 1] != nil {
                    cell.configureViewWithCurrentQuestion(option: answers[indexPath.section - 1][indexPath.row - 1]!)
                }
                
                cell.labelQuestion.text = questions[indexPath.section - 1][indexPath.row - 1]
                
                return cell
            }
        }
    }
    
}

// MARK: - UITableViewDelegate
extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let height = cellHeightsDictionary[indexPath] else {
            return UITableViewAutomaticDimension
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
}

// MARK: - ProfileQuestionCellDelegate
extension ProfileViewController: ProfileQuestionCellDelegate {
    
    func optionButtonTapped(option: Int, indexPath: IndexPath) {
        audioPlayer?.play()
        answers[indexPath.section - 1][indexPath.row - 1] = option
        profileView.tableView.reloadSections(IndexSet(integersIn: (indexPath.section - 1)...(indexPath.section)), with: .none)
    }
    
    func optionButtonTouchDownTapped(option: Int, indexPath: IndexPath) {
        generator.impactOccurred()
    }
}
