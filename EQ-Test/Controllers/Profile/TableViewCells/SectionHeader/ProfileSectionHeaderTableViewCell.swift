//
//  ProfileSectionHeaderTableViewCell.swift
//  EQ-Test
//
//  Created by Serdar Turan on 5.10.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ProfileSectionHeaderTableViewCell
class ProfileSectionHeaderTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!
    
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
