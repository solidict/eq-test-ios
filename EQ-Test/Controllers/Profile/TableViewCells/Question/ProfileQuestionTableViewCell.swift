//
//  ProfileQuestionTableViewCell.swift
//  EQ-Test
//
//  Created by Serdar Turan on 5.10.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ProfileQuestionCellDelegate
protocol ProfileQuestionCellDelegate {
    func optionButtonTapped(option: Int, indexPath: IndexPath)
    func optionButtonTouchDownTapped(option: Int, indexPath: IndexPath)
}

// MARK: - ProfileQuestionTableViewCell
class ProfileQuestionTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var labelQuestion: UILabel!
    
    @IBOutlet weak var viewOption1: UIView!
    @IBOutlet weak var labelOption1: UILabel!
    
    @IBOutlet weak var viewOption2: UIView!
    @IBOutlet weak var labelOption2: UILabel!

    @IBOutlet weak var viewOption3: UIView!
    @IBOutlet weak var labelOption3: UILabel!

    @IBOutlet weak var viewOption4: UIView!
    @IBOutlet weak var labelOption4: UILabel!

    @IBOutlet weak var viewOption5: UIView!
    @IBOutlet weak var labelOption5: UILabel!

    // MARK: - Instance Properties
    var selectedIndexPath: IndexPath?
    var selectedOption: Int = -1
    
    // MARK: - Delegate
    var delegate: ProfileQuestionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureView()
        configureLabels()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Configuration
    func configureView() {
        
        viewOption1.layer.cornerRadius = 4.0
        viewOption2.layer.cornerRadius = 4.0
        viewOption3.layer.cornerRadius = 4.0
        viewOption4.layer.cornerRadius = 4.0
        viewOption5.layer.cornerRadius = 4.0
    }
    
    func configureLabels() {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        
        let attrString1 = NSMutableAttributedString(string: "👎👎\n1")
        attrString1.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString1.length))
        labelOption1.attributedText = attrString1
        labelOption1.textAlignment = .center
        
        let attrString2 = NSMutableAttributedString(string: "👎\n2")
        attrString2.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString2.length))
        labelOption2.attributedText = attrString2
        labelOption2.textAlignment = .center
        
        let attrString3 = NSMutableAttributedString(string: "🤔\n3")
        attrString3.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString3.length))
        labelOption3.attributedText = attrString3
        labelOption3.textAlignment = .center
        
        let attrString4 = NSMutableAttributedString(string: "👍\n4")
        attrString4.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString4.length))
        labelOption4.attributedText = attrString4
        labelOption4.textAlignment = .center
        
        let attrString5 = NSMutableAttributedString(string: "👍👍\n5")
        attrString5.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString5.length))
        labelOption5.attributedText = attrString5
        labelOption5.textAlignment = .center
    }
    
    func configureViewWithCurrentQuestion(option: Int) {
        
        changeViewColor(index: -1)
        
        switch option {
        case 1:
            self.changeViewColor(index: 0)
        case 2:
            self.changeViewColor(index: 1)
        case 3:
            self.changeViewColor(index: 2)
        case 4:
            self.changeViewColor(index: 3)
        case 5:
            self.changeViewColor(index: 4)
        default:
            break
        }
    }
    
    func changeViewColor(index: Int) {
        
        let views: [UIView] = [viewOption1, viewOption2, viewOption3, viewOption4, viewOption5]
        
        if index == -1 {
            
            for view in views {
                view.backgroundColor = UIColor(red: 229.0 / 255.0, green: 229.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
            }
            
        } else {
            
            for viewIndex in 0..<views.count {
                
                if viewIndex == index {
                    views[viewIndex].backgroundColor = UIColor(red: 255.0 / 255.0, green: 204.0 / 255.0, blue: 0.0, alpha: 1.0)
                } else {
                    views[viewIndex].backgroundColor = UIColor(red: 229.0 / 255.0, green: 229.0 / 255.0, blue: 229.0 / 255.0, alpha: 1.0)
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func buttonOption1Tapped(_ sender: Any) {

        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTapped(option: 1, indexPath: indexPath)
    }
    
    @IBAction func buttonOption1TouchDownTapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTouchDownTapped(option: 1, indexPath: indexPath)
    }
    
    @IBAction func buttonOption2Tapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTapped(option: 2, indexPath: indexPath)
    }
    
    @IBAction func buttonOption2TouchDownTapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTouchDownTapped(option: 2, indexPath: indexPath)
    }
    
    @IBAction func buttonOption3Tapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTapped(option: 3, indexPath: indexPath)
    }
    
    @IBAction func buttonOption3TouchDownTapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTouchDownTapped(option: 3, indexPath: indexPath)
    }
    
    @IBAction func buttonOption4Tapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTapped(option: 4, indexPath: indexPath)
    }
    
    @IBAction func buttonOption4TouchDownTapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTouchDownTapped(option: 4, indexPath: indexPath)
    }
    
    
    @IBAction func buttonOption5Tapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTapped(option: 5, indexPath: indexPath)
    }
    
    @IBAction func buttonOption5TouchDownTapped(_ sender: Any) {
        
        guard let indexPath = selectedIndexPath else {
            return
        }
        
        delegate?.optionButtonTouchDownTapped(option: 5, indexPath: indexPath)
    }
    
}
