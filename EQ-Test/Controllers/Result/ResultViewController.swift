//
//  ResultViewController.swift
//  EQ-Test
//
//  Created by Serdar Turan on 7.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ResultViewController
class ResultViewController: UIViewController {

    var results: [ResultModel] = []
    
    // MARK: - Instance Properties
    var resultView: ResultView! {
        guard isViewLoaded else { return nil }
        return view as? ResultView
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureTableView()
        configureButton()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Configuration
    func configureTableView() {
        
        let resultTitle = UINib(nibName: "ResultTitleTableViewCell", bundle: nil)
        resultView.tableView.register(resultTitle, forCellReuseIdentifier: "ResultTitleTableViewCell")
        
        let resultMain = UINib(nibName: "ResultMainTableViewCell", bundle: nil)
        resultView.tableView.register(resultMain, forCellReuseIdentifier: "ResultMainTableViewCell")
        
        let resultSub = UINib(nibName: "ResultSubTableViewCell", bundle: nil)
        resultView.tableView.register(resultSub, forCellReuseIdentifier: "ResultSubTableViewCell")
        
        resultView.tableView.estimatedRowHeight = 20.0
        resultView.tableView.rowHeight = UITableViewAutomaticDimension
        
        resultView.EQLabel.text = "\(results[0].score) %"
        resultView.internalLabel.text = "\(results[1].score) %"
        resultView.socialLabel.text = "\(results[2].score) %"
        
        let newResults = Array(results.dropFirst(3))
        results = newResults
        
        resultView.tableView.tableFooterView = resultView.footerView
        
        
//        resultView.tableView.tableHeaderView = resultView.tableHeader
    }
    
    func configureButton() {
        
        resultView.footerView.buttonDownload.layer.borderWidth = 4.0
        resultView.footerView.buttonDownload.layer.borderColor = UIColor.black.cgColor
        
        resultView.footerView.buttonDownload.layer.shadowColor = UIColor.black.cgColor
        resultView.footerView.buttonDownload.layer.shadowOpacity = 0.4
        resultView.footerView.buttonDownload.layer.shadowOffset = CGSize.zero
        
    }
    
    // MARK: - Actions
    @IBAction func buttonHomeTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonDownloadTapped(_ sender: Any) {
        
        let alertView = UIAlertController(title: "Information", message: "Your request received.", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alertView, animated: true, completion: nil)
    }
    
}

// MARK: - UITableViewDataSource
extension ResultViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return resultView.tableHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return resultView.tableHeader.frame.size.height
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if results[indexPath.row].type == 1 {
                
                let cell = resultView.tableView.dequeueReusableCell(withIdentifier: "ResultTitleTableViewCell", for: indexPath) as! ResultTitleTableViewCell
                cell.labelTitle.text = results[indexPath.row].name
//                cell.labelPercentage.text = "\(results[indexPath.row].score) %"
                return cell
                
            } else if results[indexPath.row].type == 2 {
                
                let cell = resultView.tableView.dequeueReusableCell(withIdentifier: "ResultMainTableViewCell", for: indexPath) as! ResultMainTableViewCell
                cell.labelTitle.text = results[indexPath.row].name
                cell.labelPercentage.text = "\(results[indexPath.row].score) %"
                return cell
                
            } else {
                
                let cell = resultView.tableView.dequeueReusableCell(withIdentifier: "ResultSubTableViewCell", for: indexPath) as! ResultSubTableViewCell
                cell.labelTitle.text = results[indexPath.row].name
                cell.labelPercentage.text = "\(results[indexPath.row].score) %"
                return cell
            }
        }
    
}

// MARK: - UITableViewDelegate
extension ResultViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
