//
//  ResultSubTableViewCell.swift
//  EQ-Test
//
//  Created by Serdar Turan on 7.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ResultSubTableViewCell
class ResultSubTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPercentage: UILabel!
    
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
