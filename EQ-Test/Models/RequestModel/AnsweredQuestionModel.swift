//
//  AnsweredQuestionModel.swift
//  EQ-Test
//
//  Created by Serdar Turan on 7.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

// MARK: - AnsweredQuestionModel
struct AnsweredQuestionModel: Codable {
    
    let questionId: Int
    let answer: Int
    
    enum CodingKeys: String, CodingKey {
        case questionId
        case answer
    }
}
