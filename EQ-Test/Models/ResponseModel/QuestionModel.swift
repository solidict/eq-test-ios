//
//  QuestionModel.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

// MARK: - QuestionModel
struct QuestionModel: Codable {
    
    var id: Int
    var text: String
    var imageUrl: String
    var primary: Int?
    var secondary: Int?
    var field: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case text
        case imageUrl
        case primary
        case secondary
        case field
    }
    
}
