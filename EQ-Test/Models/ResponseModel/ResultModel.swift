//
//  ResultModel.swift
//  EQ-Test
//
//  Created by Serdar Turan on 7.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

// MARK: - ResultModel
struct ResultModel: Codable {
    
    let name: String
    let type: Int
    let score: Int
    let categoryId: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case type
        case score
        case categoryId
    }
}
