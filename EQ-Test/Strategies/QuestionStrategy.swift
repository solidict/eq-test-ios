//
//  QuestionStrategy.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

protocol QuestionStrategy: class {
            
    func advanceToNextQuestion() -> Bool
    func advanceToPreviousQuestion() -> Bool
    func currentQuestion() -> QuestionModel
    
    func questionIndexTitle() -> String
    
    func isQuestionAnswered() -> Bool
    func questionAnswered(_ answeredQuestion: AnsweredQuestionModel)
    
    func results() -> [AnsweredQuestionModel]
}
