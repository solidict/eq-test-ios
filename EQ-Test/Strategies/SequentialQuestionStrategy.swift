//
//  SequentialQuestionStrategy.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

// MARK: - SequentialQuestionStrategy
public class SequentialQuestionStrategy: QuestionStrategy {

    // MARK: - Properties
    private let questions: [QuestionModel]
    private var answeredQuestions: [AnsweredQuestionModel]
    private var questionIndex = 0
    
    // MARK: - Object Lifecycle
    init(questions: [QuestionModel]) {
        self.questions = questions
        self.answeredQuestions = []
    }
    
    // MARK: - QuestionStrategy
    func advanceToNextQuestion() -> Bool {
        
        guard questionIndex + 1 < questions.count else {
            return false
        }
        
        questionIndex += 1
        return true
    }
    
    func advanceToPreviousQuestion() -> Bool {
        
        guard questionIndex - 1 >= 0 else {
            return false
        }
        
        questionIndex -= 1
        return true
    }
    
    func currentQuestion() -> QuestionModel {
        return questions[questionIndex]
//        return questions[85]
    }
    
    func questionIndexTitle() -> String {
        return "\(questionIndex + 1)/\(questions.count)"
    }
    
    func isQuestionAnswered() -> Bool {
        
        if answeredQuestions.contains(where: { $0.questionId == questions[questionIndex].id }) {
            return true
        } else {
            return false
        }
    }
    
    func questionAnswered(_ answeredQuestion: AnsweredQuestionModel) {
        
        if let index = answeredQuestions.index(where: { $0.questionId == answeredQuestion.questionId }) {
            answeredQuestions[index] = answeredQuestion
        } else {
            answeredQuestions.append(answeredQuestion)
        }
    }
    
    func results() -> [AnsweredQuestionModel] {
        return answeredQuestions
    }
    
}
