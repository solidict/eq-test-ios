//
//  GearAnimationView.swift
//  EQ-Test
//
//  Created by Selim Savsar on 14.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit
import Gifu

class GearAnimationView: UIView {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var animationView: GIFImageView!
    @IBOutlet weak var topHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topBackgroundTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBackgroundBottomConstraint: NSLayoutConstraint!

}
