//
//  ResultView.swift
//  EQ-Test
//
//  Created by Serdar Turan on 7.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ResultView
class ResultView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeader: UIView!
    
    @IBOutlet weak var EQLabel: UILabel!
    @IBOutlet weak var internalLabel: UILabel!
    @IBOutlet weak var socialLabel: UILabel!
    @IBOutlet weak var footerView: ResultFooterView!
}
