//
//  ProfileHeaderView.swift
//  EQ-Test
//
//  Created by Serdar Turan on 4.10.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ProfileHeaderView
class ProfileHeaderView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var textFieldName: UITextField!
    
    @IBOutlet weak var labelSurname: UILabel!
    @IBOutlet weak var viewSurname: UIView!
    @IBOutlet weak var textFieldSurname: UITextField!
    
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var viewBirthDate: UIView!
    @IBOutlet weak var textFieldBirthDate: UITextField!
    
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var textFieldGender: UITextField!
    
    @IBOutlet weak var labelEducation: UILabel!
    @IBOutlet weak var viewEducation: UIView!
    @IBOutlet weak var textFieldEducation: UITextField!
}
