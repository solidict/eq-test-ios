//
//  ProfileView.swift
//  EQ-Test
//
//  Created by Serdar Turan on 4.10.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - ProfileView
class ProfileView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: ProfileHeaderView!
    @IBOutlet weak var footerView: ProfileFooterView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
