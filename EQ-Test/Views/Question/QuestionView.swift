//
//  QuestionView.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - QuestionView
class QuestionView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var labelQuestionCount: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonForward: UIButton!
    
    @IBOutlet weak var imageViewQuestion: UIImageView!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var viewOption1: UIView!
    @IBOutlet weak var viewOption2: UIView!
    @IBOutlet weak var viewOption3: UIView!
    @IBOutlet weak var viewOption4: UIView!
    @IBOutlet weak var viewOption5: UIView!
    
    @IBOutlet weak var buttonOption1: UIButton!
    @IBOutlet weak var buttonOption2: UIButton!
    @IBOutlet weak var buttonOption3: UIButton!
    @IBOutlet weak var buttonOption4: UIButton!
    @IBOutlet weak var buttonOption5: UIButton!
    
    @IBOutlet weak var labelOption1: UILabel!
    @IBOutlet weak var labelOption2: UILabel!
    @IBOutlet weak var labelOption3: UILabel!
    @IBOutlet weak var labelOption4: UILabel!
    @IBOutlet weak var labelOption5: UILabel!
    
    // MARK: - Constraint
    @IBOutlet weak var viewHeaderHeight: NSLayoutConstraint!
    
}
