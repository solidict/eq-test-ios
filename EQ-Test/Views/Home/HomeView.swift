//
//  HomeView.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

// MARK: - HomeView
class HomeView: UIView {

    // MARK: - Outlets
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var buttonFillProfile: UIButton!
    @IBOutlet weak var buttonStart: UIButton!
}
