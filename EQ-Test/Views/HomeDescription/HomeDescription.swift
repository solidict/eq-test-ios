//
//  HomeDescription.swift
//  EQ-Test
//
//  Created by Selim Savsar on 13.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import UIKit

class HomeDescription: UIView {

    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var descriptionBackground: UIView!

}
