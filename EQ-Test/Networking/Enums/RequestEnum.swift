//
//  RequestEnum.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

import Alamofire

enum RequestEnum: UrlRequest {
    
    case questions
    case evaluate(parameters: [String: Any])
    
    private static let baseUrl = "http://misc.solidict.com/eq"
    
    // MARK: UrlRequest Protocol
    var url: URL {
        var relativeUrl = RequestEnum.baseUrl
        
        switch self {
        case .questions:
            relativeUrl += "/questions"
        case .evaluate:
            relativeUrl += "/evaluate"
        }
        
        return URL(string: relativeUrl)!
    }
    
    var method: HTTPMethod {
        
        switch self {
        case .questions:
            return .get
        case .evaluate:
            return .post
        }
    }
    
    var parameters: Parameters? {
        
        switch self {
        case .questions:
            return nil
        case .evaluate(let parameters):
            return parameters
        }
    }
    
    var encoding: ParameterEncoding {
        
        switch self {
        default:
            return JSONEncoding.default
        }
    }
    
    var headers: HTTPHeaders {
        
        var dictionary = [String: String]()
        dictionary["Cache-Control"] = "max-age=0"
        dictionary["Connection"] = "Keep-Alive"
        dictionary["Content-Type"] = "application/json"
        return dictionary
    }
}
