//
//  RequestError.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation

enum RestError: Error {
    case statusCode(statusCode: Int)
    case logicError(error: Error)
}
