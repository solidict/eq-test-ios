//
//  NetworkClient.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation
import Alamofire

class NetworkClient {
    
    // MARK: - Questions
    public static func getQuestions(completionHandler: @escaping ApiRouter.CompletionHandler<[QuestionModel]>) {
        let request = RequestEnum.questions
        ApiRouter.doRequest(request: request, decodingType: [QuestionModel].self, completionHandler: completionHandler)
    }
    
    // MARK: - Evaluate
    public static func evaluate(answeredQuestions: [AnsweredQuestionModel], completionHandler: @escaping ApiRouter.CompletionHandler<[ResultModel]>) {
        
        var answers: [[String: Any]] = []
        for answer in answeredQuestions {
            let answerDict: [String: Any] = ["questionId": answer.questionId, "answer": answer.answer]
            answers.append(answerDict)
        }
        
        let parameters : [String: Any] = ["answers": answers]

        let request = RequestEnum.evaluate(parameters: parameters)
        ApiRouter.doRequest(request: request, decodingType: [ResultModel].self, completionHandler: completionHandler)
    }
    
}
