//
//  ApiRouter.swift
//  EQ-Test
//
//  Created by Serdar Turan on 6.09.2018.
//  Copyright © 2018 Serdar Turan. All rights reserved.
//

import Foundation
import Alamofire

protocol UrlRequest {
    var url: URL { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var headers: HTTPHeaders { get }
}

class ApiRouter {
    typealias CompletionHandler<T> = (_ error: RestError?, _ response: T?) -> Void
    
    public static func doRequest<T: Decodable>(request: UrlRequest, decodingType: T.Type, completionHandler: @escaping CompletionHandler<T>) {
        Alamofire.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding, headers: request.headers)
            .responseData { (response) in
                switch response.result {
                case .success(let value):
                    #if DEBUG
                    // Convert response into string
                    let jsonString = String(data: value, encoding: .utf8)!
                    print("JSON Response = \(jsonString)")
                    #endif
                    
                    // Check if status code is not between 200 and 300
                    if !(200..<300).contains(response.response!.statusCode) {
                        completionHandler(RestError.statusCode(statusCode: response.response!.statusCode), nil)
                        
                        return
                    }
                    
                    // Decode and return
                    let decoder = JSONDecoder()
                    
                    do {
                        let instance = try decoder.decode(decodingType, from: value)
                        
                        completionHandler(nil, instance)
                    } catch let error {
                        completionHandler(RestError.logicError(error: error), nil)
                    }
                case .failure(let error):
                    completionHandler(RestError.logicError(error: error), nil)
                }
        }
    }
}
